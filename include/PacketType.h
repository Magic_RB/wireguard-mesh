//
// Created by root on 9/24/18.
//

#ifndef WIREGUARD_MESH_PACKETTYPE_H
#define WIREGUARD_MESH_PACKETTYPE_H


enum PacketType {
    Greetings = 0,
    Update = 1,
    Info = 2,
};


#endif //WIREGUARD_MESH_PACKETTYPE_H
