//
// Created by main on 9/24/18.
//

#ifndef WIREGUARD_MESH_WG_H
#define WIREGUARD_MESH_WG_H

#include <string>
#include <vector>
#include "Peer.h"
#include <Port.h>

class Wg {
public:
    explicit Wg(std::string interface);

    void set_endpoint_for_pubkey(std::string pubkey, Ip ip, Port port);
    void update();
    std::vector<Peer*> all_peers_with_endpoint();

    std::vector<Peer> Peers;
    std::vector<Peer> PeersWithEndpoint;
    std::string Interface;
    std::string PublicKey;
    Ip InterfaceIp;
};


#endif //WIREGUARD_MESH_WG_H
