//
// Created by main on 9/24/18.
//

#ifndef WIREGUARD_MESH_PEER_H
#define WIREGUARD_MESH_PEER_H


#include <string>
#include "Ip.h"
#include "Port.h"

// @TODO Add a method to convert a peer into a packet, is this will be later used to send packet over UDP, signature: Packet to_packet(PacketType packetType); void from_packet();

class Peer {
public:
    Peer();
    explicit Peer(std::string str);
    Peer(std::string publicKey, Ip endpoint, Port port);

    void from_wg_peer(std::string str);
    std::string get_string();

    std::string PublicKey;
    Ip Endpoint;
    Port Port;
};


#endif //WIREGUARD_MESH_PEER_H
