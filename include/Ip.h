//
// Created by main on 9/24/18.
//

#ifndef WIREGUARD_MESH_IP_H
#define WIREGUARD_MESH_IP_H


#include <stdexcept>

class Ip {
public:
    Ip();
    Ip(unsigned char a, unsigned char b, unsigned char c, unsigned char d);
    explicit Ip(std::string str);

    unsigned char operator[] (unsigned char index);

    std::string get_string();

    void set(unsigned char a, unsigned char b, unsigned char c, unsigned char d);
    void set(std::string str);

private:
    unsigned char _bytes[4];
};


#endif //WIREGUARD_MESH_IP_H
