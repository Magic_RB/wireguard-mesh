//
// Created by root on 9/24/18.
//

#ifndef WIREGUARD_MESH_PACKET_H
#define WIREGUARD_MESH_PACKET_H


#include <string>
#include <vector>
#include <array>
#include "Ip.h"
#include "Port.h"
#include "PacketType.h"

class Packet {
public:
    Packet();
    Packet(PacketType packetType, std::string publicKey, Ip endpoint, Port port);
    Packet(PacketType packetType, std::string publicKey, std::string origin, Ip endpoint, Port port);


    PacketType PacketType;
    std::string PublicKey;
    std::string Origin;
    Ip Endpoint;
    Port Port;

    std::string to_string();
    void from_byte_array(std::array<char, 95> bytes);
    std::array<char, 95> to_byte_array();
};


#endif //WIREGUARD_MESH_PACKET_H
