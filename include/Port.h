//
// Created by root on 9/24/18.
//

#ifndef WIREGUARD_MESH_PORT_H
#define WIREGUARD_MESH_PORT_H


class Port {
public:
    Port();
    explicit Port(unsigned short s);
    Port(unsigned char b1, unsigned char b2);

    union {
        unsigned short AsShort;
        unsigned char AsBytes[2];
    };
};


#endif //WIREGUARD_MESH_PORT_H
