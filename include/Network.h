//
// Created by root on 9/25/18.
//

#ifndef WIREGUARD_MESH_NETWORK_H
#define WIREGUARD_MESH_NETWORK_H

#include <netinet/in.h>
#include "Port.h"
#include "Wg.h"

class Network {
public:
    int SocketFD;
    sockaddr_in LocalAdress = {};
// @TODO Add a constructor to specify the network interface name
    Wg wg = Wg("wg0");
    Port Port;

    ~Network();

    void Intitialize(struct Port port);
    void DispatchGreetings();
};


#endif //WIREGUARD_MESH_NETWORK_H
