cmake_minimum_required(VERSION 3.12)
project(wireguard_mesh)

include_directories(include)

set(CMAKE_CXX_STANDARD 17)

add_executable(wireguard_mesh src/main.cpp src/Peer.cpp include/Peer.h src/Ip.cpp include/Ip.h src/Wg.cpp include/Wg.h src/Packet.cpp include/Packet.h src/Port.cpp include/Port.h include/PacketType.h src/Network.cpp include/Network.h)