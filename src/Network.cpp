//
// Created by root on 9/25/18.
//

#include <sys/socket.h>
#include <string>
#include <Packet.h>
#include "Network.h"

Network::~Network()
{
    if (SocketFD <= 0)
        shutdown(SocketFD, 2);
}

void Network::Intitialize(Port port)
{
    SocketFD = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (SocketFD < 0)
        throw std::string("Failed to open socket!");

    LocalAdress.sin_family = AF_INET;
    LocalAdress.sin_port = htons(port.AsShort);
    LocalAdress.sin_addr.s_addr = htons(INADDR_ANY);

    if (bind(SocketFD, (struct sockaddr*)&LocalAdress, sizeof(LocalAdress)) < 0)
        throw std::string("Failed to bind to port " + std::to_string(port.AsShort)  + "!");
}

void Network::DispatchGreetings()
{
    if (SocketFD <= 0)
        return;

    for (Peer& peer : wg.PeersWithEndpoint) {
        Packet packet = Packet(PacketType::Greetings, wg.PublicKey, wg.InterfaceIp, Port);

        sockaddr_in remote_address = {};

        remote_address.sin_family = AF_INET;
        remote_address.sin_port = Port.AsShort;
        //remote_address.sin_addr.s_addr = htons(peer.Endpoint.get_string().c_str());

        sendto(SocketFD, packet.to_byte_array().data(), 95, 0, )
    }
}