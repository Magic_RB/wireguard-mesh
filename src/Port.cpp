//
// Created by root on 9/24/18.
//

#include "Port.h"

Port::Port() = default;

Port::Port(unsigned short s)
{
    AsShort = s;
}

Port::Port(unsigned char b1, unsigned char b2)
{
    AsBytes[0] = b1;
    AsBytes[1] = b2;
}