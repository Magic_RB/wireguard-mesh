//
// Created by main on 9/24/18.
//

#include <iostream>
#include <Port.h>
#include <net/if.h>
#include <arpa/inet.h>
#include "Wg.h"


Wg::Wg(std::string interface)
{
    this->Interface = interface;

    FILE *cmd = popen(("wg show " + interface + " endpoints").c_str(), "r");

    char buffer[256];
    std::string wg_output;

    while (fgets(buffer, sizeof(buffer), cmd) != nullptr)
    {
        wg_output += buffer;
    }
    pclose(cmd);

    size_t pos;

    while ((pos = wg_output.find('\n')) != std::string::npos) {
        std::string line = std::string(wg_output.substr(0, pos));
        std::string fixed_line;

        for (char& ch : line) {
            if (ch == '\t')
                fixed_line += "    ";
            else
                fixed_line += ch;
        }

        PeersWithEndpoint.emplace_back(Peer(fixed_line));

        wg_output.erase(0, pos + 1);
    }

    cmd = popen(("wg show " + interface + " peers").c_str(), "r");

    while (fgets(buffer, sizeof(buffer), cmd) != nullptr)
    {
        wg_output += buffer;
    }
    pclose(cmd);

    while ((pos = wg_output.find('\n')) != std::string::npos) {
        std::string line = std::string(wg_output.substr(0, pos));

        line += "    0.0.0.0:0";

        Peers.emplace_back(Peer(line));

        wg_output.erase(0, pos + 1);
    }

    popen(("wg show " + interface + " pubkey").c_str(), "r");
    while (fgets(buffer, sizeof(buffer), cmd) != nullptr)
    {
        wg_output += buffer;
    }
    pclose(cmd);

    PublicKey = wg_output.substr(0, wg_output.size() - 2);

    int fd;
    ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);

    ifr.ifr_addr.sa_family = AF_INET;

    strncpy(ifr.ifr_name, Interface.c_str(), IFNAMSIZ);

    shutdown(fd, 2);

    InterfaceIp = Ip(std::string(inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr)));
}

void Wg::set_endpoint_for_pubkey(std::string pubkey, Ip ip, Port port)
{
    for (Peer& peer : Peers) {
        if (peer.PublicKey == pubkey) {
            peer.Endpoint = ip;
            peer.Port.AsShort = port.AsShort;
        }
    }
}

void Wg::update()
{
    for (Peer& peer : Peers) {
        system(("wg set " + Interface + " peer " + peer.PublicKey + " Endpoint " + peer.Endpoint.get_string() + ":" + std::to_string(peer.Port.AsShort)).c_str());
    }
}

std::vector<Peer*> Wg::all_peers_with_endpoint()
{
    std::vector<Peer*> peers_with_endpoint;

    for (Peer& peer : Peers) {
        if (peer.Endpoint)
    }
}