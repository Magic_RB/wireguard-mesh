//
// Created by main on 9/24/18.
//

#include "../include/Peer.h"

Peer::Peer() = default;

Peer::Peer(std::string str)
{
    from_wg_peer(std::move(str));
}

Peer::Peer(std::string publicKey, Ip endpoint, class Port port)
{
    PublicKey = std::move(publicKey);
    Endpoint = endpoint;
    Port = port;
}

void Peer::from_wg_peer(std::string str)
{
    PublicKey = str.substr(0, 44);
    str.erase(0, 43 + 4);

    Endpoint.set(str.substr(0, str.find(':')));
    str.erase(0, str.find(':') + 1);

    Port.AsShort = (unsigned short)(std::stoi((str)));
}

std::string Peer::get_string()
{
    return std::string(PublicKey + "    " + Endpoint.get_string() + ":" + std::to_string(Port.AsShort));
}
