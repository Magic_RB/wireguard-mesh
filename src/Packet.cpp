//
// Created by root on 9/24/18.
//

#include <array>
#include <Packet.h>

Packet::Packet()
{
    PacketType = PacketType::Greetings;
}

Packet::Packet(enum PacketType packetType, std::string publicKey, Ip endpoint, class Port port)
{
    PacketType = packetType;
    PublicKey = std::move(publicKey);
    Origin = PublicKey;
    Endpoint = endpoint;
    Port = port;
}

Packet::Packet(enum PacketType packetType, std::string publicKey, std::string origin, Ip endpoint, class Port port)
{
    PacketType = packetType;
    PublicKey = std::move(publicKey);
    Origin = std::move(origin);
    Endpoint = endpoint;
    Port = port;
}

std::string Packet::to_string()
{
    return std::to_string(PacketType) + ": " + PublicKey + "  " + Origin + "    " + Endpoint.get_string() + ":" + std::to_string(Port.AsShort);
}

void Packet::from_byte_array(std::array<char, 95> bytes)
{
    PacketType = static_cast<enum PacketType>(bytes[0]);

    PublicKey.clear();
    Origin.clear();

    for (short i = 1; i < 45; i++)
        PublicKey += bytes[i];

    for (short i = 45; i < 89; i++)
        Origin += bytes[i];

    Endpoint.set(bytes[89], bytes[90], bytes[91], bytes[92]);

    Port.AsBytes[0] = bytes[93];
    Port.AsBytes[1] = bytes[94];
}

std::array<char, 95> Packet::to_byte_array()
{
    std::array<char, 95> array = std::array<char, 95>();
    short index = 1;

    array[0] = PacketType;

    for (char& ch : PublicKey) {
        array[index] = ch;
        index++;
    }

    for (char& ch : Origin) {
        array[index] = ch;
        index++;
    }

    array[index] = Endpoint[0];
    array[index + 1] = Endpoint[1];
    array[index + 2] = Endpoint[2];
    array[index + 3] = Endpoint[3];

    index += 4;

    array[index] = Port.AsBytes[0];
    array[index + 1] = Port.AsBytes[1];

    return array;
}