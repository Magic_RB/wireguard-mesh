//
// Created by main on 9/24/18.
//

#include "../include/Ip.h"

Ip::Ip() = default;

Ip::Ip(unsigned char a, unsigned char b, unsigned char c, unsigned char d){
    set(a, b, c, d);
}

Ip::Ip(std::string str)
{
    set(std::move(str));
}

unsigned char Ip::operator[] (unsigned char index)
{
    if (index < 4)
        return _bytes[index];
    throw std::out_of_range("Ip::at(): index is greater then 4");
}

std::string Ip::get_string()
{
    return std::string(std::to_string(_bytes[0]) + '.' + std::to_string(_bytes[1]) + '.' + std::to_string(_bytes[2]) + '.' + std::to_string(_bytes[3]));
}

void Ip::set(unsigned char a, unsigned char b, unsigned char c, unsigned char d)
{
    _bytes[0] = a;
    _bytes[1] = b;
    _bytes[2] = c;
    _bytes[3] = d;
}

void Ip::set(std::string str)
{
    size_t pos = 0;

    char index = 0;
    std::string tokens[4];


    while ((pos = str.find('.')) != std::string::npos) {
        tokens[index] = str.substr(0, pos);

        str.erase(0, pos + 1);

        index++;
    }

    tokens[3] = str;

    _bytes[0] = (unsigned char)std::stoi(tokens[0]);
    _bytes[1] = (unsigned char)std::stoi(tokens[1]);
    _bytes[2] = (unsigned char)std::stoi(tokens[2]);
    _bytes[3] = (unsigned char)std::stoi(tokens[3]);
}